import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CepController, CepService } from './cep';

@Module({
  imports: [],
  controllers: [AppController, CepController],
  providers: [AppService, CepService],
})
export class AppModule {}
